"""
ai'nasa
Copyright 2015 by Jorge Martinez Santiago, Jose Manuel Ciges Regueiro, David Constenla Rodriguez and Adam Anh Doan Kim Carames.
All rights reserved.

This project remains the property of Jorge Martinez Santiago, Jose Manuel Ciges Regueiro, David Constenla Rodriguez and Adam Anh Doan Kim Carames. Dissemination, reproduction or transmission of any part
of this project in any form or by any means, including copying, by electronic or mechanical methods is strictly
forbidden unless prior written permission is obtained from ai'nasa team.
"""

from flask import Flask

from flask.ext.cors import CORS
from flask.ext.mongoengine import MongoEngine
from flask.ext.mongorest import MongoRest
from flask.ext.mongorest.views import ResourceView
from flask.ext.mongorest.resources import Resource
from flask.ext.mongorest import operators as ops
from flask.ext.mongorest import methods  
import datetime

app = Flask(__name__)

app.config.update(
    MONGODB_HOST = 'localhost',
    MONGODB_PORT = 27017,
    MONGODB_DB = 'ainasa',
)

db = MongoEngine(app)
api = MongoRest(app)

cors = CORS(app, resources={r"*": {"origins": "*"}})

class GPS(db.EmbeddedDocument):
    lat = db.FloatField()
    lng = db.FloatField()

class GPSResource(Resource):
    document = GPS

class Detection(db.EmbeddedDocument):
    height = db.FloatField()
    width = db.FloatField()
    
class DetectionResource(Resource):
    document = Detection

class Nasa(db.Document):
    name = db.StringField()
    last_log = db.DateTimeField(default=datetime.datetime.now)
    last_in = db.DateTimeField(default=datetime.datetime.now)
    last_out = db.DateTimeField(default=datetime.datetime.now)
    gps = db.EmbeddedDocumentField(GPS)
    detection = db.EmbeddedDocumentField(Detection)

class NasaHistory(db.Document):
    nasa_id = db.StringField()
    log_date = db.DateTimeField(default=datetime.datetime.now)
    last_in = db.DateTimeField(default=datetime.datetime.now)
    last_out = db.DateTimeField(default=datetime.datetime.now)
    gps = db.EmbeddedDocumentField(GPS)
    detection = db.EmbeddedDocumentField(Detection)
    

class NasaResource(Resource):
    document = Nasa
    related_resources = {
        'detection': DetectionResource,
        'gps' : GPSResource
    }
    filters = {
        'id': [ops.Exact]
    }

class NasaHistoryResource(Resource):
    document = NasaHistory
    related_resources = {
        'detection': DetectionResource,
        'gps' : GPSResource
    }
    filters = {
        'nasa_id': [ops.Exact]
    }

@api.register(name='nasa', url='/nasa/')
class NasaView(ResourceView):
    resource = NasaResource
    methods = [methods.Create, methods.Update, methods.Fetch, methods.List]

@api.register(name='nasaHistory', url='/nasa-history/')
class NasaHistoryView(ResourceView):
    resource = NasaHistoryResource
    methods = [methods.Create, methods.Update, methods.Fetch, methods.List]
    
if __name__ == '__main__':
    app.run(host='0.0.0.0')